// Source code is decompiled from a .class file using FernFlower decompiler.
package framework;

import entities.Shoe;

public interface ShoeFactory {
   Shoe createShoe(String var1, String var2, String var3, double var4);
}
