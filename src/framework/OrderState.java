// Source code is decompiled from a .class file using FernFlower decompiler.
package framework;

import entities.Order;

public interface OrderState {
   void next(Order var1);

   void prev(Order var1);

   void printStatus();

   String getStatus();
}
