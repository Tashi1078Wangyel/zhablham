// Source code is decompiled from a .class file using FernFlower decompiler.
package framework;

import entities.Order;

public interface Observer {
   void notify(Order var1);

   void update(Order var1);
}
