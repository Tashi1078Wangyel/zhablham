// Source code is decompiled from a .class file using FernFlower decompiler.
package framework;

public class PaymentProcessor {
   private PaymentStrategy strategy;

   public PaymentProcessor() {
   }

   public void setPaymentStrategy(PaymentStrategy strategy) {
      this.strategy = strategy;
   }

   public boolean executePayment(double amount) {
      if (this.strategy != null) {
         return this.strategy.pay(amount);
      } else {
         System.out.println("Payment strategy is not set.");
         return false;
      }
   }
}
